/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.robotproject2;

/**
 *
 * @author Administrator
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap map;

    public Robot(int x, int y, char symbol, TableMap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (walkNorth()) {
                    return false;
                }
                break;
            case 'S':
            case 's':
                if (walkSouth()) {
                    return false;
                }
                break;
            case 'E':
            case 'd':
                if (walkEast()) {
                    return false;
                }
                break;
            case 'W':
            case 'a':
                if (walkWest()) {
                    return false;
                }
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if (map.isBomb(x, y)) {
            System.out.println("Founded Bomb!!! (" + x + ", " + y + ")");
        }
    }

    private boolean walkWest() {
        if (map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkEast() {
        if (map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkSouth() {
        if (map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkNorth() {
        if (map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
